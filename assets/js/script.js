	// $("li").on("click", function(){
	// 	if($(this).css("color") === "rgb(128, 128, 128)"){
	// 		$(this).css({
	// 		  color: "black",
	// 		  textDecoration: "none"
	// 		})
	// 	} else {s
	// 		$(this).css({
	// 		  color: "gray",
	// 		  textDecoration: "line-through"
	// 		})
	// 	}
	// });

$("ul").on("click", "li", function(){
	$(this).toggleClass("done");
})

$("ul").on("click", "li span", function(event){
	$(this).parent().fadeOut(500, function(){
		$(this).remove();
	});
	event.stopPropagation();
})

$("input[type='text']").on("keypress", function(event){
 if(event.which === 13) {
     var todoText = $(this).val()
     $(this).val("");
     $("ul").append("<li><span class='list'><i class='far fa-trash-alt'></i></span> " + todoText + "</li>");
    }
})

$("#plus").on("click", function(){
	$("input[type='text']").fadeToggle(500);
})

   
